#define RIPPLE_MAX_SLOTS 8

precision mediump float;

attribute vec3 position;
attribute vec4 color;

uniform mat4 worldViewProjection;

uniform float ripple_centers[RIPPLE_MAX_SLOTS * 2];
uniform float ripple_deltas[RIPPLE_MAX_SLOTS];

varying vec3 vColor;
varying vec3 vRipple;
varying float vDistance;

void main()
{
    vec4 p = vec4(position, 1.0);

    float ripple = 0.0;
    for(int i = 0; i < RIPPLE_MAX_SLOTS; i++){
        float delta = ripple_deltas[i];
        if(delta == 0.0){
            continue;
        }
        float dist = length(p.xz - vec2(ripple_centers[i * 2], ripple_centers[i * 2 + 1]));
        ripple += max(0.0, (sin(dist * -1.0 + delta * 8.0) * 10.0 - 9.5) / max(1.0, (dist * dist * dist)));
    }
    p.y += ripple;

    vColor = color.rgb;

    gl_Position = worldViewProjection * p;
    vDistance = gl_Position.z / 30.0;
    vRipple = vec3(0.102 * 5.0, 0.698 * 5.0, 0.91 * 5.0) * ripple;
}