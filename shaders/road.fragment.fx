precision mediump float;

uniform float time;
uniform float alpha;

varying vec2 vUv;

void main()
{
    float intensity = (sin(vUv.y * 20.0 + time * 5.0) * 50.0 - 40.0) * (sin(vUv.x * 3.1415) - 0.5);

    gl_FragColor = vec4(vec3(0.4, 0.4, 0.4), 0.8 * clamp(intensity, 0.0, 1.0) * alpha);
}