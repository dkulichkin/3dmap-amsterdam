precision mediump float;

attribute vec3 position;
attribute vec4 color;

uniform mat4 worldViewProjection;

varying vec3 vColor;

void main()
{
    vec4 p = vec4(position, 1.0);

    vColor = color.rgb;

    gl_Position = worldViewProjection * p;
}