#extension GL_OES_standard_derivatives : enable

#define LINE_WIDTH 1.25
#define BASE_COLOR vec4(0.01, 0.01, 0.4, 0.5)
#define WIRE_COLOR vec4(0.6, 0.8, 1.0, 0.5)
#define DISTANCE_COLOR vec4(0.4, 0.1, 0.5, 0.0)

precision mediump float;

varying vec3 vColor;
varying vec3 vRipple;
varying float vDistance;

float edgeFactorTri()
{
   vec3 d = fwidth(vColor.xyz);
   vec3 a3 = smoothstep(vec3(0.0), d * LINE_WIDTH, vColor.xyz);
   return min(min(a3.x, a3.y), a3.z);
}

void main()
{
    gl_FragColor = mix(mix(WIRE_COLOR, BASE_COLOR, edgeFactorTri()), DISTANCE_COLOR, vDistance);
    gl_FragColor.rgb += vRipple;
}