'use strict';

if (!process.env.BROWSER) require('dotenv').load();

const gps_top_left = process.env.NW.split(',').map((coord) => +coord),
			gps_bottom_right = process.env.SE.split(',').map((coord) => +coord),
			zoom_level = process.env.ZOOM_LEVEL,
			gps_center = [
				(gps_top_left[0] + gps_bottom_right[0]) / 2,
				(gps_top_left[1] + gps_bottom_right[1]) / 2
			]
;

module.exports = {

	gpsToTileCoords: function(lat, lon, zoom)
	{
		zoom = zoom || zoom_level;

		var x = (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
		var y = (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));

		return {x: x, y: y};
	},

	tileCoordsToGps: function(x, y, zoom)
	{
		zoom = zoom || zoom_level;

		var lon = (x/Math.pow(2,zoom)*360-180);
		var n = Math.PI - 2 * Math.PI * y / Math.pow(2, zoom);
		var lat = (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));

		return {lat: lat, lon: lon};
	},

	gpsToModelCoords: function(lat, lon)
	{
		var x = (lon - gps_center[1]) * 1114.9332780329	- 24.5308643000;
		var y = (gps_center[0] - lat) * -1112.9632793718 - 2.9473349000;

		return {x: x, y: y};
	}

};
