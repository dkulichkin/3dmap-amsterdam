'use strict';

import Utils from '../Utils';
import earcut from 'earcut';

var loader = null;

class MapzenLoaderWorker
{
	constructor(properties)
	{
		this.loaded_building_ids = {};
		this.loaded_road_ids = {};
		this.road_properties = properties['road_properties'];
	}

	loadData(data, tile_id)
	{
		var buildings = this.loadBuildings(data['buildings']);
		var roads = this.loadRoads(data['roads']);

		var response = {
			common_buildings: buildings.common,
			roads: roads,
			tile_id: tile_id
		};

		postMessage(response);
	}

	loadBuildings(data)
	{
		var common_vertex_data = {
			positions: [],
			indices: [],
			colors: []
		};


		for(let feature_index = 0, l = data['features'].length; feature_index < l; feature_index++)
		{
			let feature = data['features'][feature_index];
			let geometry = feature['geometry'];
			let properties = feature['properties'];
			let id = properties['id'];

			if(this.loaded_building_ids.hasOwnProperty(id)) continue;

			this.loaded_building_ids[id] = true;

			let vertex_data = common_vertex_data;

			var height = properties.hasOwnProperty('height') ? (properties['height'] / 100.0 + 0.1) : 0.1;
			var min_height = properties.hasOwnProperty('min_height') ? (properties['min_height'] / 100.0 + 0.1) : 0.0;

			if(geometry['type'] !== 'Polygon' && geometry['type'] !== 'MultiPolygon')
			{
				self.console.log('Invalid geometry type ' + geometry['type']);
				continue;
			}

			var earcut_data = {
				vertices: [],
				holes: [],
				polygon_indices: []
			};

			for(let polygon_index = 0; polygon_index < geometry['coordinates'].length; polygon_index++)
			{
				let coords = geometry['coordinates'][polygon_index];
				let last_c = null;

				if (polygon_index) earcut_data.holes.push(earcut_data.vertices.length / 2);

				for(let coord_index = 0; coord_index < coords.length; coord_index++)
				{
					let c = Utils.gpsToModelCoords(coords[coord_index][1], coords[coord_index][0]);

					earcut_data.vertices.push(c.x);
					earcut_data.vertices.push(c.y);
					earcut_data.polygon_indices.push(polygon_index);

					if(last_c)
					{
						vertex_data.positions.push(last_c.x, height, last_c.y);
						vertex_data.positions.push(c.x, min_height, c.y);
						vertex_data.positions.push(last_c.x, min_height, last_c.y);

						vertex_data.positions.push(last_c.x, height, last_c.y);
						vertex_data.positions.push(c.x, height, c.y);
						vertex_data.positions.push(c.x, min_height, c.y);

						vertex_data.colors.push(0, 1, 1, 1);
						vertex_data.colors.push(1, 0, 1, 1);
						vertex_data.colors.push(0, 0, 0, 1);

						vertex_data.colors.push(0, 1, 1, 1);
						vertex_data.colors.push(0, 0, 0, 1);
						vertex_data.colors.push(1, 0, 1, 1);

						vertex_data.indices.push(vertex_data.indices.length);
						vertex_data.indices.push(vertex_data.indices.length);
						vertex_data.indices.push(vertex_data.indices.length);

						vertex_data.indices.push(vertex_data.indices.length);
						vertex_data.indices.push(vertex_data.indices.length);
						vertex_data.indices.push(vertex_data.indices.length);
					}

					last_c = c;
				}
			}

			let top_indices = earcut(earcut_data.vertices, earcut_data.holes, 2);

			for(var i = 0; i < top_indices.length; i += 3)
			{
				let i1 = top_indices[i];
				let i2 = top_indices[i + 1];
				let i3 = top_indices[i + 2];

				let e1 = earcut_data.polygon_indices[i1] == earcut_data.polygon_indices[i2] && (i1 - i2) == 1;
				let e2 = earcut_data.polygon_indices[i2] == earcut_data.polygon_indices[i3] && (i2 - i3) == 1;
				let e3 = earcut_data.polygon_indices[i3] == earcut_data.polygon_indices[i1] && (i3 - i1) == 1;

				vertex_data.positions.push(earcut_data.vertices[i1 * 2], height, earcut_data.vertices[i1 * 2 + 1]);
				vertex_data.positions.push(earcut_data.vertices[i2 * 2], height, earcut_data.vertices[i2 * 2 + 1]);
				vertex_data.positions.push(earcut_data.vertices[i3 * 2], height, earcut_data.vertices[i3 * 2 + 1]);

				let c0 = [1, 1, 1, 1];
				let c1 = [1, 1, 1, 1];
				let c2 = [1, 1, 1, 1];

				if(e1){
					c0[0] = 0;
					c1[0] = 0;
				}

				if(e2){
					c1[1] = 0;
					c2[1] = 0;
				}

				if(e3){
					c2[2] = 0;
					c0[2] = 0;
				}

				vertex_data.colors.push(c0[0], c0[1], c0[2], c0[3]);
				vertex_data.colors.push(c1[0], c1[1], c1[2], c1[3]);
				vertex_data.colors.push(c2[0], c2[1], c2[2], c2[3]);

				vertex_data.indices.push(vertex_data.indices.length);
				vertex_data.indices.push(vertex_data.indices.length);
				vertex_data.indices.push(vertex_data.indices.length);
			}
		}

		return {
			common: common_vertex_data
		};
	}

	loadRoads(data)
	{
		var mesh_data = [];

		for(let feature_index = 0; feature_index < data['features'].length; feature_index++)
		{
			let feature = data['features'][feature_index];
			let geometry = feature['geometry'];
			let properties = feature['properties'];
			let id = properties['id'];
			let kind_properties = this.road_properties[properties['kind']];

			if(!this.road_properties.hasOwnProperty(properties['kind'])) continue;

			if (geometry['type'] == 'LineString')
			{
				let path = [];

				let coords = geometry['coordinates'];

				for(let coord_index = 0; coord_index < coords.length; coord_index++)
				{
					let c = Utils.gpsToModelCoords(coords[coord_index][1], coords[coord_index][0]);
					path.push(c.x, 0.0, c.y);
				}

				mesh_data.push({
					id: id,
					path: path,
					properties: kind_properties
				})
			}
			else if(geometry['type'] == 'MultiLineString')
			{
				for (let path_index = 0; path_index < geometry['coordinates'].length; path_index++)
				{
					let path = [];
					let coords = geometry['coordinates'][path_index];

					for(let coord_index = 0; coord_index < coords.length; coord_index++)
					{
						let c = Utils.gpsToModelCoords(coords[coord_index][1], coords[coord_index][0]);
						path.push(c.x, 0.0, c.y);
					}

					mesh_data.push({
						id: id + '_' + path_index,
						path: path,
						properties: kind_properties
					});
				}
			}
			else
			{
				self.console.log('Invalid geometry type ' + geometry['type']);
			}
		}

		return mesh_data;
	}
}

self.onmessage = function(e)
{
	if(!loader)
	{
		loader = new MapzenLoaderWorker(e.data);
	}
	else
	{
		loader.loadData(e.data['data'], e.data['tile_id']);
	}
}

postMessage("Ready");
