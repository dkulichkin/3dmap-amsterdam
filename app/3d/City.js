'use strict';

import {CircleEase, EasingFunction, Animation} from 'babylonjs';
import MapzenLoader from './MapzenLoader';
import Utils from '../Utils';

class City
{
	constructor(renderer)
	{
		/**
		 * @type Renderer
		 */
		this.renderer = null;

		/**
		 * @type MapzenLoader
		 */
		this.loader = null;

		this.on_complete_load = null;
		this.on_tile_load = null;
		this.tiles_requested = [];
		this.tiles_loaded_count = 0;

		const gps_top_left = process.env.NW.split(',').map((coord) => +coord),
					gps_bottom_right = process.env.SE.split(',').map((coord) => +coord),
					zoom_level = process.env.ZOOM_LEVEL,
					gps_center = [
						(gps_top_left[0] + gps_bottom_right[0]) / 2,
						(gps_top_left[1] + gps_bottom_right[1]) / 2
					]
		;

		console.log(Utils.gpsToModelCoords(gps_top_left[0], gps_top_left[1]));
		console.log(Utils.gpsToModelCoords(gps_bottom_right[0], gps_bottom_right[1]));
		console.log(Utils.gpsToModelCoords(gps_center[0], gps_center[1]));

		this.renderer = renderer;
		this.loader = new MapzenLoader(renderer.scene);
		this.socket = window.map_socket;

		this.tile_top_left = Utils.gpsToTileCoords(gps_top_left[0], gps_top_left[1], zoom_level);
		this.tile_bottom_right = Utils.gpsToTileCoords(gps_bottom_right[0], gps_bottom_right[1], zoom_level);
		this.tile_center = Utils.gpsToTileCoords(gps_center[0], gps_center[1], zoom_level);
		this.total_tiles_count = (this.tile_bottom_right.x - this.tile_top_left.x + 1) * (this.tile_bottom_right.y - this.tile_top_left.y + 1);
	}

	load(on_places_load, on_tile_load, on_complete_load)
	{
		var _this = this;
		this.on_complete_load = on_complete_load;
		this.on_tile_load = on_tile_load;

		this.loadPlaces().then(function()
		{
			on_places_load.call(_this);
			_this.loadTile(_this.tile_center.x, _this.tile_center.y - 1);
		});
	}

	loadPlaces()
	{
		var _this = this;
		return new Promise(function(resolve)
		{
			_this.socket.on("places-response", () => resolve());
			_this.socket.emit("places-request");
		});
	}

	loadTile(x,y)
	{
		var _this = this;
		var key = x + '_' + y;

		if(this.tiles_requested.hasOwnProperty(key)) return;

		if(x < this.tile_top_left.x || x > this.tile_bottom_right.x || y < this.tile_top_left.y || y > this.tile_bottom_right.y) return;

		this.tiles_requested[key] = true;

		this.loader.load(x, y, function(common_mesh, road_mesh)
		{
			common_mesh.material = _this.renderer.materials.city_material;
			common_mesh.addLODLevel(40, null);

			var keys = [
				{
					frame: 0,
					value: 50.0
				},
				{
					frame: 120,
					value:0.0
				}
			];

			var ease = new CircleEase();
			ease.setEasingMode(EasingFunction.EASINGMODE_EASEOUT);
			var animation = new Animation('City_common_loading_' + x + '_' + y, 'position.y', 30, Animation.ANIMATIONTYPE_FLOAT, Animation.ANIMATIONLOOPMODE_CONSTANT);
			animation.setKeys(keys);
			animation.setEasingFunction(ease);
			common_mesh.animations.push(animation);
			_this.renderer.scene.beginAnimation(common_mesh, 0, 120);

			keys = [
				{
					frame: 0,
					value: 0.0
				},
				{
					frame: 30,
					value: 1.0
				}
			];

			animation = new Animation('City_roads_loading_' + x + '_' + y, 'alpha', 30, Animation.ANIMATIONTYPE_FLOAT, Animation.ANIMATIONLOOPMODE_CONSTANT);
			animation.setKeys(keys);
			road_mesh.animations.push(animation);
			_this.renderer.scene.beginAnimation(road_mesh, 0, 120);
			road_mesh.material = _this.renderer.materials.road_material;

			_this.loadTile(x - 1, y);
			_this.loadTile(x + 1, y);
			_this.loadTile(x, y - 1);
			_this.loadTile(x, y + 1);

			_this.tiles_loaded_count++;

			if(_this.on_tile_load)
			{
				_this.on_tile_load(_this.tiles_loaded_count, _this.total_tiles_count);
			}

			if(_this.tiles_loaded_count == _this.total_tiles_count)
			{
				_this.on_complete_load.call(this);
			}
		});
	}
}

export default City;
