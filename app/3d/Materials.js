'use strict';

import {ShaderMaterial, StandardMaterial, Color3, Color4, Texture} from 'babylonjs';

class Materials
{
	/**
	 * @param Renderer
	 */
	constructor(renderer)
	{
		this.renderer = renderer;

		/**
		 * @type BABYLON.ShaderMaterial
		 */
		this.city_material = this.createCityMaterial();

		/**
		 * @type BABYLON.ShaderMaterial
		 */
		this.ground_material = this.createGroundMaterial();

		/**
		 * @type BABYLON.ShaderMaterial
		 */
		this.building_material = this.createBuildingMaterial();

		/**
		 * @type BABYLON.ShaderMaterial
		 */
		this.road_material = this.createRoadMaterial();

		/**
		 * @type BABYLON.ShaderMaterial
		 */
		this.marker_material = this.createMarkerMaterial();

	}

	createCityMaterial()
	{
		var shaderMaterial = new ShaderMaterial(
			"city", this.renderer.scene, '/shaders/city',
			{
				attributes: ["position", "color"],
				uniforms: ["worldViewProjection", "ripple_centers", "ripple_deltas"],
				needAlphaBlending: true
			}
		);

		shaderMaterial.backFaceCulling = true;

		return shaderMaterial;
	}

	createGroundMaterial()
	{
		var ground_material = new StandardMaterial("Ground", this.renderer.scene);
		ground_material.diffuseColor = new Color3(0.0, 0.0, 0.2);
		ground_material.specularColor = new Color3(0.0, 0.0, 0.0);

		return ground_material;
	}

	createBuildingMaterial()
	{
		var _this = this;
		var shaderMaterial = new ShaderMaterial(
			"building", this.renderer.scene, '/shaders/building',
			{
				attributes: ["position", "color"],
				uniforms: ["worldViewProjection", "baseColor", "wireColor"],
				needAlphaBlending: true
			}
		);

		shaderMaterial.setColor4("baseColor", new Color4(0.4, 0.4, 0.01, 0.5));
		shaderMaterial.setColor4("wireColor", new Color4(1.0, 0.8, 0.6, 0.5));
		shaderMaterial.backFaceCulling = true;

		shaderMaterial.onBind = function(material, mesh)
		{
			if(mesh.hasOwnProperty('building_color'))
			{
				let color = mesh['building_color'];
				let delta = 0.8 + Math.sin(_this.renderer.time * 2.0) * 0.4;
				material._effect.setFloat4("baseColor", color.r * delta, color.g * delta, color.b * delta, 0.8);
				material._effect.setFloat4("wireColor", color.r * 2, color.g * 2, color.b * 2, 0.5);
			}
		};

		return shaderMaterial;
	}

	createRoadMaterial()
	{
		var shaderMaterial = new ShaderMaterial(
			"road", this.renderer.scene, '/shaders/road',
			{
				attributes: ["position", "uv"],
				uniforms: ["worldViewProjection", "time", "alpha"],
				needAlphaBlending: true
			}
		);

		shaderMaterial.setFloat("time", 0);
		shaderMaterial.setFloat("alpha", 0);
		shaderMaterial.backFaceCulling = true;

		shaderMaterial.onBind = function(material, mesh)
		{
			if(mesh.hasOwnProperty('alpha'))
			{
				material._effect.setFloat("alpha", mesh['alpha']);
			}
		};

		return shaderMaterial;
	}

	createMarkerMaterial()
	{
		var shaderMaterial = new ShaderMaterial(
				"marker", this.renderer.scene, '/shaders/marker',
				{
					attributes: ["position", "uv"],
					uniforms: ["worldViewProjection", "highlight"],
					samplers: ["tex"],
					needAlphaBlending: true
				}
		);

		shaderMaterial.setFloat("time", 0.0);

		var tex = new Texture(require("file!../../public/images/marker.png"), this.renderer.scene);
		shaderMaterial.setTexture("tex", tex);
		shaderMaterial.backFaceCulling = false;

		shaderMaterial.onBind = function(material, mesh)
		{
			if(mesh.hasOwnProperty('highlight'))
			{
				material._effect.setFloat("highlight", mesh['highlight']);
			}
			else
			{
				material._effect.setFloat("highlight", 0.0);
			}
		};

		return shaderMaterial;
	}

	update(delta, time)
	{
		this.road_material.setFloat("time", time);
	}
}

export default Materials;