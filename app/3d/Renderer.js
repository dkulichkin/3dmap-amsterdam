'use strict';

import {Engine, Scene, Color3, Vector3, FreeCamera, DirectionalLight} from 'babylonjs';
import City from './City';
import Materials from './Materials';
import CameraController from './CameraController';

class Renderer
{
	constructor(container)
	{
		/**
		 * @type BABYLON.Engine
		 */
		this.engine = null;

		/**
		 * @type BABYLON.Scene
		 */
		this.scene = null;

		/**
		 * @type BABYLON.Camera
		 */
		this.basic_camera = null;

		/**
		 * @type BABYLON.Camera
		 */
		this.vr_camera = null;

		/**
		 * @type City
		 */
		this.city = null;

		/**
		 * @type Materials
		 */
		this.materials = null;

		/**
		 * @type CameraController
		 */
		this.camera_controller = null;

		this.canvas = document.createElement("canvas");
		this.canvas.id = 'renderCanvas';
		container.appendChild(this.canvas);

		this.engine = new Engine(this.canvas, true);
		this.engine.renderEvenInBackground = true;
		this.engine.enableOfflineSupport = false;
		//this.engine.setHardwareScalingLevel(0.125);

		this.time = 0.0;

		this.initEventHandlers();
		this.loadScene();
	}

	initEventHandlers()
	{
		window.addEventListener('resize', () => this.engine.resize());
		this.canvas.addEventListener('contextmenu', (e) => e.preventDefault());
	}

	loadScene()
	{
		var _this = this;
		var scene = new Scene(this.engine);
		var light = new DirectionalLight("Light", new Vector3(-1.0, -1.0, -1.0), scene);

		scene.clearColor = new Color3(0.0, 0.0, 0.0);
		scene.activeCamera = new FreeCamera("Camera", new Vector3(0.0, 10.0, 0.0), scene);
		scene.activeCamera.speed = 0.5;
		scene.ambientColor = new Color3(1.0, 1.0, 1.0);

		this.scene = scene;
		this.camera_controller = new CameraController(this);
		this.forbidAutoCamera();
		this.loadMaterials();
		this.loadCity();

		scene.activeCamera.attachControl(_this.canvas);
		scene.activeCamera.position.x = 0.0;
		scene.activeCamera.position.y = 10.0;
		scene.activeCamera.position.z = -10.0;
		scene.activeCamera.setTarget(new Vector3(0.0, 3.0, 0.0));
		scene.beforeRender = function(){
			var delta = _this.engine.getDeltaTime() / 1000.0;
			_this.time += delta;
			light.direction = scene.activeCamera.getTarget().subtract(scene.activeCamera.position).normalize();
			_this.update(delta, _this.time);
		};

		this.run();
	}

	update(delta, time)
	{
		this.camera_controller.update(delta, time);
		this.materials.update(delta, time);
		this.scene._onPointerMove({clientX: this.scene.pointerX, clientY: this.scene.pointerY});
	}

	loadCity()
	{
		var _this = this;
		this.city = new City(this);
		this.city.load(
			function(){},
			function()
			{
				if(_this.city.tiles_loaded_count === 20) _this.allowAutoCamera(false);
			},
			function(){}
		);
	}

	loadMaterials()
	{
		this.materials = new Materials(this);
	}

	allowAutoCamera(start_now)
	{
		this.camera_controller.allow_automatic = true;

		if(start_now) this.camera_controller.startAutomatic();
	}

	forbidAutoCamera()
	{
		this.camera_controller.allow_automatic = false;
	}

	run()
	{
		this.engine.runRenderLoop(() => this.scene && this.scene.render());
	}
}


export default Renderer;
