'use strict';

import pako from 'pako';
import {Mesh, VertexData, Vector3} from 'babylonjs';

class MapzenLoader
{
	/**
	 * @param BABYLON.Scene
	 */
	constructor(scene)
	{
		var _this = this;
		var indexedDB = window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

		this.scene = scene;

		this.road_properties = {
			highway: {
				width: 20
			},
			major_road: {
				width: 10
			},
			minor_road: {
				width: 5
			}
		};

		var MyWorker = require("worker!./MapzenLoaderWorker.js");

		this.worker_ready = false;
		this.worker = new MyWorker();
		this.worker.onmessage = function(e)
		{
			_this.worker_ready = e.data == 'Ready';
			_this.worker.postMessage({ road_properties: _this.road_properties });
		};

		this.layers = process.env.LAYERS.split(',');

		this.queue = [];
		this.queue_data = {};

		this.max_loading = 4;
		this.current_loading = 0;
		this.socket = null;

		this.db = null;
		this.db_loading = false;

		this.socket = window.map_socket;
		this.socket.on("map-tile-response", function(resp)
		{
			try
			{
				var restored = JSON.parse(pako.inflate(resp['data'], {to: 'string'}));
				var tile_id = resp['x'] + '_' + resp['y'] + '_' + resp['zoom'];
				var params = _this.queue_data[tile_id];

				if (!params)
				{
					console.log("Wrong tile response: " + resp);
					return;
				}

				if (_this.db)
				{
					let transaction = _this.db.transaction(['tiles'], 'readwrite');
					transaction.objectStore('tiles').add({key: tile_id, data: restored});
				}

				_this.processRawTileData(tile_id, restored);
			}
			catch(e)
			{
				return;
			}
		});

		if (indexedDB)
		{
			let db_request = indexedDB.open("3dmap-amsterdam", 1);
			_this.db_loading = true;
			db_request.onerror = function()
			{
				console.log('Failed to open IndexedDB');
				_this.db_loading = false;
			};

			db_request.onupgradeneeded = function(event)
			{
				_this.db = event.target.result;
				_this.db.createObjectStore('tiles', {keyPath: 'key'});
				_this.db_loading = false;
			};

			db_request.onsuccess = function(e) {
				_this.db = event.target.result;
				_this.db_loading = false;
			};
		}
	}

	load(x, y, onload)
	{
		var tile_id = x + '_' + y + '_' + process.env.ZOOM_LEVEL;

		this.queue_data[tile_id] = {
			x: x,
			y: y,
			onload: onload
		};

		this.queue.push(tile_id);

		this.load_next();
	}

	load_next()
	{
		var _this = this;

		if(!this.queue.length) return;

		if(this.current_loading >= this.max_loading) return;

		if(!this.worker_ready || this.db_loading)
		{
			setTimeout(() => this.load_next(), 100);
			return;
		}

		var tile_id = this.queue.shift();
		var params = this.queue_data[tile_id];

		this.worker.onmessage = function(e)
		{
			_this.processTileData(e.data);
			delete _this.queue_data[e.data['tile_id']];
			_this.current_loading--;
			_this.load_next();
		};

		this.current_loading++;

		var tile_request = {x: params.x, y: params.y, zoom: process.env.ZOOM_LEVEL};

		if(this.db)
		{
			var request = this.db.transaction(["tiles"], "readwrite").objectStore("tiles").get(tile_id);
			request.onsuccess = function()
			{
				if (request.result)
				{
					_this.processRawTileData(tile_id, request.result.data);
				}
				else
				{
					_this.socket.emit("map-tile-request", tile_request);
				}
			};
		}
		else
		{
			_this.socket.emit("map-tile-request", tile_request);
		}
	}

	processTileData(data)
	{
		var params = this.queue_data[data['tile_id']];
		var common_buildings = this.create_common_buildings_mesh('Block_' + params.x + '_' + params.y + '_buildings', data['common_buildings']);
		var roads = this.create_roads_mesh('Block_' + params.x + '_' + params.y + '_roads', data['roads']);

		if(params.onload) params.onload(common_buildings, roads);
	}

	processRawTileData(tile_id, data)
	{
		this.worker.postMessage({
			type: 'data',
			data: data,
			tile_id: tile_id
		});
	}

	create_building_mesh(name, data)
	{
		var mesh = new Mesh(name, this.scene);
		var vertex_data = new VertexData();

		vertex_data.positions = data.positions;
		vertex_data.indices = data.indices;
		vertex_data.colors = data.colors;
		vertex_data.applyToMesh(mesh, false);

		mesh.alphaIndex = 2000;

		return mesh;
	}

	create_common_buildings_mesh(name, data)
	{
		var mesh = this.create_building_mesh(name, data);
		mesh.alphaIndex = 1500;
		return mesh;
	}

	create_roads_mesh(name, data)
	{
		var shape = [ new Vector3(0.005, 0.0, 0.0), new Vector3(-0.005, 0.0, 0.0) ];
		var meshes = [];

		for(var i = 0; i < data.length; i++)
		{
			let road = data[i];
			let path = [];

			for(let offset = 0; offset < road.path.length; offset += 3)
			{
				path.push(Vector3.FromArray(road.path, offset));
			}

			let mesh = Mesh.ExtrudeShape(name + "_" + road.id, shape, path, road.properties.width, 0, 0, this.scene);
			meshes.push(mesh);
		}

		var result_mesh;

		if(meshes.length)
		{
			result_mesh = Mesh.MergeMeshes(meshes, true);
		}
		else
		{
			result_mesh = new Mesh(name, this.scene);
		}

		result_mesh.name = name;
		result_mesh.alphaIndex = 1000;

		return result_mesh;
	}

}

export default MapzenLoader
