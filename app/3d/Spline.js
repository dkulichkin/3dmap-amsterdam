'use strict';

import {Vector3} from 'babylonjs';

class Spline
{
	constructor(points)
	{
		this.points = points;
	}

	getPoint(t)
	{
		var point_abs = this.points.length * t;
		var point_index = Math.floor(point_abs);
		var weight = point_abs - point_index;

		point_index = point_index || this.points.length;

		var a = this.points[(point_index - 1) % this.points.length];
		var b = this.points[point_index % this.points.length];
		var c = this.points[(point_index + 1) % this.points.length];
		var d = this.points[(point_index + 2) % this.points.length];

		return Vector3.CatmullRom(a, b, c, d, weight);
	}
}

export default Spline;
