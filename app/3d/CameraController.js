'use strict';

import {QuadraticEase, EasingFunction, Vector3} from 'babylonjs';
import Spline from './Spline';
import Utils from '../Utils';
import superagent from 'superagent';

class CameraController
{
	constructor(renderer)
	{
		/**
		 * @type Renderer
		 */
		this.renderer = null;

		/**
		 * @type Spline
		 */
		this.spline = null;

		/**
		 * @type BABYLON.EasingFunction
		 */
		this.ease = null;

		/**
		 * @type BABYLON.Vector3
		 */
		this.last_interaction_pos = null;

		/**
		 * @type BABYLON.Vector3
		 */
		this.last_interaction_target = null;

		this.auto_delay = 10.0;
		this.auto_time = 0.0;
		this.auto_speed = 0.005;
		this.cam_height = 2.0;
		this.last_interaction_time = 0.0;
		this.automatic = false;
		this.allow_automatic = true;
		this.mouse_down = false;
		this.cam_path = [];

		var _this = this;

		this.renderer = renderer;

		this.ease = new QuadraticEase();
		this.ease.setEasingMode(EasingFunction.EASINGMODE_EASEINOUT);

		superagent.get('/json/cam_path.json').end(function(err, data)
		{
			data = JSON.parse(data.text.toString());
			for(let i = 0; i < data.length; i++)
			{
				let point = data[i];
				let pos = Utils.gpsToModelCoords(point[0], point[1]);
				let height = point[2] || 0.0;
				let vector = new Vector3(pos.x, height, pos.y);
				_this.cam_path.push(vector);
			}

			_this.spline = new Spline(_this.cam_path);
			_this.initEventHandlers();
		});
	}

	initEventHandlers()
	{
		var _this = this;

		window.addEventListener('keydown', function()
		{
			if(_this.automatic)
			{
				_this.triggerInteraction();
				_this.cancelAutomatic();
			}
		});

		_this.renderer.canvas.addEventListener('mousedown', function()
		{
			_this.cancelAutomatic();
			_this.mouse_down = true;
		});

		_this.renderer.canvas.addEventListener('mousewheel', function()
		{
			_this.cancelAutomatic();
			_this.triggerInteraction();
		});

		_this.renderer.canvas.addEventListener('keypress', function()
		{
			_this.cancelAutomatic();
			_this.triggerInteraction();
		});

		_this.renderer.canvas.addEventListener('mouseup mouseout', function()
		{
			_this.mouse_down = false;
		});
	}

	triggerInteraction()
	{
		this.last_interaction_time = this.renderer.time;
	}

	update(delta, time)
	{
		var camera = this.renderer.scene.activeCamera;

		if(this.mouse_down){
			this.triggerInteraction();
		}

		if (!this.allow_automatic) this.triggerInteraction();

		if ((time - this.last_interaction_time) >= this.auto_delay)
		{
			if (!this.automatic)
			{
				this.last_interaction_pos = camera.position.clone();
				this.last_interaction_target = camera.getTarget().clone();
			}

			//this.automatic = true;

			var cam_pos = this.spline.getPoint(this.auto_time * this.auto_speed);
			cam_pos.y = this.cam_height;

			var cam_target = this.spline.getPoint((this.auto_time + 6.0) * this.auto_speed);
			cam_target.y = 1.0;

			var mix = this.ease.ease(Math.min(((this.renderer.time - this.last_interaction_time) - this.auto_delay) / 2.0, 1.0));

			camera.position = Vector3.Lerp(this.last_interaction_pos, cam_pos, mix);
			camera.lockedTarget = Vector3.Lerp(this.last_interaction_target, cam_target, mix);
			camera.setTarget(camera.lockedTarget);

			this.auto_time += delta * 2.0;
		}

		camera.position = Vector3.Clamp(camera.position, new Vector3(-50, 1.0, -50), new Vector3(50, 50.0, 50));
	}

	cancelAutomatic()
	{
		if (this.renderer.scene.activeCamera.lockedTarget)
		{
			this.renderer.scene.activeCamera.setTarget(this.renderer.scene.activeCamera.lockedTarget);
			this.renderer.scene.activeCamera.lockedTarget = null;
		}

		this.automatic = false;
	}

	startAutomatic()
	{
		this.cancelAutomatic();
		this.last_interaction_time = this.renderer.time - this.auto_delay;
	}
}

export default CameraController;
