'use strict';

require('../public/style.css');

import io from 'socket.io-client';
import Renderer from './3d/Renderer';

window.map_socket = io(process.env.SERVER_HOST + ':' + process.env.SERVER_PORT);

export default new Renderer(document.body);
