'use strict';

import dotenv from 'dotenv';
import path from 'path';
import express from 'express';
import http from 'http';
import pako from 'pako';
import socket from 'socket.io';
import socketiostream from 'socket.io-stream';
import fs from 'fs';

dotenv.load();

const app = express();
const server = http.createServer(app);
const io = socket(server);
const port = process.env.SERVER_PORT;

app.use('/public', express.static(path.join(__dirname, '/build')));
app.use('/json', express.static(path.join(__dirname, '/json')));
app.use('/shaders', express.static(path.join(__dirname, '/shaders')));

app.get('/', (req, res) => res.sendFile(__dirname + '/index.html'));

io.on('connection', function(socket)
{
	socket.on('error', (err) => console.log(err));
	socket.on('places-request', () => socket.emit('places-response', {}));
	socket.on('map-tile-request', function(data)
	{
		var filename = [data.zoom, data.x, data.y].join('-') + '.json';
		/*var stream = socketiostream.createStream();
		socketiostream(socket).emit('map-tile-response', stream, {});
		fs.ReadStream(__dirname + '/' + filename).pipe(stream);*/
		fs.readFile(__dirname + '/data/' + filename, function(err, file)
		{
			if (err) throw err;
			var resp = {
				data: pako.deflate(file.toString(), { to: 'string' }),
				x: data.x,
				y: data.y,
				zoom: data.zoom
			};
			socket.emit('map-tile-response', resp);
		});
	});
});

server.listen(port, () => console.log(`Server listening on port ${port}`));
