'use strict';

require('dotenv').load();

var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');

var webpackConfig = {
	entry: path.join(__dirname, 'app/index.js'),
	output: {
		path: path.join(__dirname, 'build'),
		publicPath: '/public/',
		filename: 'bundle.js'
	},
	resolve: {
		extensions: ['', '.js'],
		alias: {
			babylonjs: 'babylonjs/babylon.max'
		}
	},
	module: {
		loaders: [
			{ test: /babylonjs/, loader: 'exports?BABYLON' },
			{ test: /\.(png|jpg)$/, loader: 'file-loader?name=images/[name].[ext]' },
			{ test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
			{ test: /\.js$/, exclude: /node_modules/, loader: 'babel', query: { presets: ['es2015'] } },
			{ test: /\.json$/, loader: 'json-loader'},
			{ test: /\.(ttf|eot|svg|woff|woff2)$/, loader: 'file-loader?name=fonts/[name].[ext]' }
		]
	},
	node: {
		setImmediate: false
	},
	plugins: [
		new CleanWebpackPlugin(['build']),
		new webpack.NoErrorsPlugin(),
		new ExtractTextPlugin("style.css"),
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV),
				DEBUG: JSON.stringify(process.env.DEBUG),
				BROWSER: JSON.stringify(true),
				SERVER_HOST: JSON.stringify(process.env.SERVER_HOST),
				SERVER_PORT: JSON.stringify(process.env.SERVER_PORT),
				NW: JSON.stringify(process.env.NW),
				SE: JSON.stringify(process.env.SE),
				ZOOM_LEVEL: JSON.stringify(process.env.ZOOM_LEVEL),
				LAYERS: JSON.stringify(process.env.ZOOM_LEVEL),
				SHADERS_URI: JSON.stringify(process.env.SHADERS_URI)
			}
		})
	],
	devtool: 'inline-source-map'
};

module.exports = webpackConfig;
