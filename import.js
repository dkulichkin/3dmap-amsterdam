'use strict';

var _ = require('lodash');
var fs = require('fs-extra');
var dotenv = require('dotenv');
var superagent = require('superagent');
var Progress = require('progress');
var Utils = require('./app/Utils');

dotenv.load();

const z = process.env.ZOOM_LEVEL,
			layers = process.env.LAYERS,
			api_key = process.env.API_KEY,
		 	nw = process.env.NW.split(','),
			se = process.env.SE.split(','),
			nwTileCoords = Utils.gpsToTileCoords(+nw[0], +nw[1], z),
			seTileCoords = Utils.gpsToTileCoords(+se[0], +se[1], z),
			x_start = nwTileCoords.x, x_end = seTileCoords.x,
			y_start = nwTileCoords.y, y_end = seTileCoords.y,
			total = (x_end - x_start + 1) * (y_end - y_start + 1)
;

var x = x_start, y = y_start;

var bar = new Progress(`Pulling tiles [${nwTileCoords.x},${nwTileCoords.y}] / [${seTileCoords.x},${seTileCoords.y}] ----- :current of :total`, {total: total});

var downloadFile = function(x, y, z)
{
	var url = `http://vector.mapzen.com/osm/${layers}/${z}/${x}/${y}.json?api_key=${api_key}`;
	return new Promise(function(resolve, reject)
	{
		superagent.get(url)
			.on('error', (err) => reject())
			.on('end', (err) => resolve())
			.pipe(fs.createWriteStream(__dirname + `/data/${z}-${x}-${y}.json`));
	});
};

var callNext = function()
{
	var x_old = x;

	if (y > y_end) return;

	downloadFile(x, y, z).then(function()
	{
		bar.tick();
		callNext();
	});

	x = (x_old === x_end) ? x_start : x + 1;
	y = (x_old === x_end) ? y + 1 : y;
};

fs.removeSync(__dirname + '/data');
fs.mkdirsSync(__dirname + '/data');

_.each(_.range(0, 50), callNext);
